from bcc import BPF
import psutil
import pymysql.cursors
import os
from termcolor import colored

is_blacklisted = False
curr_process_name = ""
process_list = []

#Connect to mySQL container
connection = pymysql.connect(host='172.17.0.2',
                             user='maintainer',
                             password='rezilionftw123',
                             database='blacklist',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)

def getList():
    with connection.cursor() as cursor:
        sql = "SELECT process FROM processlist"
        cursor.execute(sql)
        result = cursor.fetchall() #Read all rows from process table
        for grp in result:
            process_list.append(list(grp.values())[0]) #Add process names to process list

getList() #Initial data

#Get PID of processes calling packet_bind to sniff packets
prog = """
#include <net/sock.h>  
#include <net/inet_sock.h>  
#include <bcc/proto.h>

struct data_t
{
    u32 pid;
}; 

BPF_PERF_OUTPUT(events);

int get_type(struct pt_regs *ctx, struct socket* sock)
{   
    struct data_t data = {};
    data.pid = bpf_get_current_pid_tgid();

    events.perf_submit(ctx, &data, sizeof(data));
    return 0;
}"""

b = BPF(text=prog) #Initialize BPF object
b.attach_kprobe(event = "packet_bind", fn_name="get_type")

def checkIfProcessRunning(processName):
    '''
    Check if there is any running process that contains the given name processName.
    '''
    #Iterate over all available processes
    for proc in psutil.process_iter():
        try:
            # Check if the name matches
            if processName.lower() in proc.name().lower():
                return True
        except (psutil.NoSuchProcess, psutil.AccessDenied):
            pass
    return False

def print_event(cpu, data, size):
    event = b["events"].event(data)
    process_name = psutil.Process(event.pid).name() #Get process name
    
    #Set globals for usage in case of prompt
    global curr_process_name
    global is_blacklisted

    #Find whether wireshark/tshark is activating dumpcap to sniff packets & kill blacklisted processes
    if(process_name == "dumpcap"):
        if(checkIfProcessRunning("wireshark")):
            curr_process_name = "wireshark"
            if ("wireshark" in process_list):
                is_blacklisted = True
                os.system("killall wireshark")
                print("PID: %d, Program: %s, Sniffing program: %s -" % (event.pid, process_name, "wireshark") + colored(" Process Terminated", 'red'))
            else:
                is_blacklisted = False
                print("PID: %d, Program: %s, Sniffing program: %s" % (event.pid, process_name, "wireshark"))
        elif(checkIfProcessRunning("tshark")):
            curr_process_name = "tshark"
            if ("tshark" in process_list):
                is_blacklisted = True
                os.system("killall tshark")
                print("PID: %d, Program: %s, Sniffing program: %s -" % (event.pid, process_name, "tshark") + colored(" Process Terminated", 'red'))
            else: 
                is_blacklisted = False
                print("PID: %d, Program: %s, Sniffing program: %s" % (event.pid, process_name, "tshark"))
    else:
        curr_process_name = process_name
        if(process_name in process_list):
            is_blacklisted = True
            os.system("killall %s" % (process_name))
            print("PID: %d, Program: %s -" % (event.pid, process_name) +  + colored(" Process Terminated", 'red'))
        else:
            is_blacklisted = False
            print("PID: %d, Program: %s" % (event.pid, process_name)) #Print sniffing process data

b["events"].open_perf_buffer(print_event)
while True:
    try:
        while True:
            b.perf_buffer_poll()
    except KeyboardInterrupt:
        resp = input("\nQuit/Add process to blacklist/Remove from blacklist? [Q]uit/[A]dd/[R]emove - ")
        if(resp == 'Q' or resp == 'q'):
            connection.close() #Close SQL connection
            break
        elif(resp == 'A' or resp == 'a'):
            if(is_blacklisted == False):
                with connection.cursor() as cursor:
                    #Add process name
                    sql = "INSERT INTO processlist (process) VALUES (%s)"
                    cursor.execute(sql, (curr_process_name))
                connection.commit()
                process_list.append(curr_process_name) 
                continue
            else:
                print(colored("Process is blacklisted already!", 'red'))
                continue
        elif (resp == 'R' or resp == 'r'):
            if(is_blacklisted == True):
                with connection.cursor() as cursor:
                    #Remove process name
                    sql = "DELETE FROM processlist WHERE process=%s"
                    cursor.execute(sql, (curr_process_name))
                connection.commit()
                process_list.remove(curr_process_name)
                continue
            else:
                print(colored("Process not blacklisted!", 'red'))
                continue
        else:
            print(colored("Invalid input!", 'red'))
            continue
